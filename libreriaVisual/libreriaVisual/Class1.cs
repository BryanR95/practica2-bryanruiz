﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libreriaVisual
{
    public class libreriaVisual
    {
        public void ordenInverso(int[] v)
        {
            Console.WriteLine("\t\t****Vector con orden inverso****");
            for (int i = v.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(v[i]);
            }

        }

        public void posicionImpares(int[] v)
        {
            Console.WriteLine("\t\t****Vector con posiciones impares****");
            for (int i = 0; i < v.Length; i++)
            {
                if (v[i] % 2 != 0)
                {
                    Console.WriteLine("\nEl numero impar introducido " + v[i] + " esta en la posicion " + (i + 1));
                }
            }

        }

        public void posicionPares(int[] v)
        {
            Console.WriteLine("\t\t****Vector con posiciones pares****");
            for (int i = 0; i < v.Length; i++)
            {
                if (v[i] % 2 == 0)
                {
                    Console.WriteLine("\nEl numero par introducido " + v[i] + " esta en la posicion " + (i + 1));
                }
            }

        }

        public void visualizarVector(int[] v)
        {
            Console.WriteLine("\t\t****Visualizar vector****");
            for (int i = 0; i < v.Length; i++)
            {
                Console.WriteLine("Posicion: " + (i + 1) + " Valor: " + v[i]);
            }
        }
    }
}
