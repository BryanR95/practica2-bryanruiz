package main;

import java.util.Scanner;

import libreriaEclipse.LibreriaEclipse;


public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int dimension; int opcion; int divisibles;
		
		
		System.out.println("\t\t\t*****MENU*****");
		System.out.println("\t\t****************************");
		System.out.println("1.\tModificar valores del vector. ");
		System.out.println("2\tVisualizar valores pares. ");
		System.out.println("3.\tVisualizar valores impares. ");
		System.out.println("4.\tVisualizar la suma de los divisibles para n. ");
		
		System.out.print("\n\tElige una opcion : ");
		opcion=in.nextInt();
		
		while(opcion<=0 || opcion>4){
			System.out.println("Error, opcion entre 1 y 9.");
			System.out.print("Vuelve a introducir una opcion: ");
			opcion=in.nextInt();
		}
		
		System.out.println("Dimension del vector: ");
		dimension=in.nextInt();
		int v[]=new int [dimension];
		LibreriaEclipse.rellenarVector(v);
		
		if(opcion==1){
			String respuesta="no";
			do{
				LibreriaEclipse.modificarVector(v);
				System.out.println("Quiere moficicar otro valor? ");
				in.nextLine();
				respuesta=in.nextLine();
				
			}
			while(respuesta.equalsIgnoreCase("si"));
		}
		if(opcion==2){
			LibreriaEclipse.visualizarValoresPares(v);
			
		}
		if(opcion==3){
			LibreriaEclipse.visualizarValoresImpares(v);
		}
		if(opcion==4){
			divisibles=LibreriaEclipse.visualizarSumaDivisibles(v);
			System.out.println("La suma de los divisibles es: "+divisibles);
		}
	}
}




