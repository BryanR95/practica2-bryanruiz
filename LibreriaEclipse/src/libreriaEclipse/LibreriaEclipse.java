package libreriaEclipse;

import java.util.Scanner;

public class LibreriaEclipse {
	
	public static int visualizarSumaDivisibles(int[] v) {
		
		Scanner in=new Scanner(System.in);
		int num;  int sumaDivisibles = 0;
		System.out.println("Introduce un numero: ");
		num=in.nextInt();
		for(int i=0;i<v.length;i++){
			if(num%v[i]==0){
				sumaDivisibles+=v[i];
			}
		}
		return sumaDivisibles;
	}

	public static void visualizarValoresImpares(int[] v) {
		System.out.println("\t\t****Valores impares: ****");
		for(int i=0;i<v.length;i++){
			if(v[i]%2!=0){
				System.out.println(v[i]);
			}
		}
		
	}

	public static void visualizarValoresPares(int[] v) {
		System.out.println("\t\t****Valores pares: ****");
		for(int i=0;i<v.length;i++){
			if(v[i]%2==0){
				System.out.println(v[i]);
			}
		}
		
	}

	public static void modificarVector(int[] v) {
		Scanner in=new Scanner(System.in);
		int nuevoValor; int viejoValor; 
		System.out.println("Que valor quiere modificar? ");
		viejoValor=in.nextInt();
		System.out.println("Que valor nuevo quiere introducir? ");
		nuevoValor=in.nextInt();
		
		boolean comprobar=false;
		for(int i=0;i<v.length;i++){
			if(viejoValor==v[i]){
				v[i]=nuevoValor;
				comprobar=true;
			}
		}
		if(comprobar==false){
			System.out.println("No se ha modificado ningun valor. ");
		}
		else{
			System.out.println("Se ha modificado el valor correctamente.");
		}
		visualizarVector(v);
		
	}

	public static void visualizarVector(int[] v) {
		System.out.println("\t\t****Visualizar Vector****");
		for(int i=0;i<v.length;i++){
			System.out.println("Posicion: "+(i+1)+" Valor: "+v[i]);
		}
	}

	public static void rellenarVector(int[] v) {
		Scanner in=new Scanner(System.in);
		for(int i=0;i<v.length;i++){
			System.out.print("Posicion: "+(i+1)+" Valor: ");
			v[i]=in.nextInt();
			while(v[i]<=0){
				System.out.println("Error, el valor tiene que ser mayor que 0.");
				System.out.print("Vuelve a introducir un valor: ");
				v[i]=in.nextInt();
			}
		}
	}
}

