package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Insets;
import javax.swing.JTabbedPane;
import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.border.MatteBorder;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Toolkit;

public class Ventana_Aplicacion extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana_Aplicacion frame = new Ventana_Aplicacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana_Aplicacion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana_Aplicacion.class.getResource("/images/edit-file256_25239.png")));
		setTitle("Altas, bajas y modificaci\u00F3n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 790, 537);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Nuevo");
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Abrir...");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Modificar");
		mnNewMenu.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_9 = new JMenuItem("Guardar");
		mnNewMenu.add(mntmNewMenuItem_9);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Guardar como...");
		mnNewMenu.add(mntmNewMenuItem_3);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Imprimir");
		mnNewMenu.add(mntmNewMenuItem_4);
		
		JMenu mnNewMenu_3 = new JMenu("Enviar");
		mnNewMenu.add(mnNewMenu_3);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("P\u00E1gina por correo electronico...");
		mnNewMenu_3.add(mntmNewMenuItem_5);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("V\u00EDnculo por correo electr\u00F3nico...");
		mnNewMenu_3.add(mntmNewMenuItem_6);
		
		JMenuItem mntmNewMenuItem_7 = new JMenuItem("Acceso directo a escritorio");
		mnNewMenu_3.add(mntmNewMenuItem_7);
		
		JMenuItem mntmNewMenuItem_8 = new JMenuItem("Importar y exportar...");
		mnNewMenu.add(mntmNewMenuItem_8);
		
		JMenuItem mntmPropiedades = new JMenuItem("Propiedades");
		mnNewMenu.add(mntmPropiedades);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mnNewMenu.add(mntmCerrar);
		
		JMenu mnNewMenu_1 = new JMenu("Edici\u00F3n");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_10 = new JMenuItem("Cortar");
		mntmNewMenuItem_10.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
		mnNewMenu_1.add(mntmNewMenuItem_10);
		
		JMenuItem mntmNewMenuItem_11 = new JMenuItem("Copiar");
		mntmNewMenuItem_11.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		mnNewMenu_1.add(mntmNewMenuItem_11);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mntmPegar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
		mnNewMenu_1.add(mntmPegar);
		
		JMenuItem mntmSeleccionarTodo = new JMenuItem("Seleccionar todo");
		mntmSeleccionarTodo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
		mnNewMenu_1.add(mntmSeleccionarTodo);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar...");
		mntmBuscar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
		mnNewMenu_1.add(mntmBuscar);
		
		JMenu mnNewMenu_2 = new JMenu("Ver");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmPantallaCompleta = new JMenuItem("Pantalla completa");
		mntmPantallaCompleta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0));
		mnNewMenu_2.add(mntmPantallaCompleta);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setMargin(new Insets(0, 125, 0, 0));
		toolBar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		toolBar.setBounds(10, 11, 709, 41);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/new-file_40454.png")));
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/folderalong_carpeta_12907.png")));
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/guardar.png")));
		toolBar.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/guardar1.png")));
		toolBar.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/print_46933.png")));
		toolBar.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/Undo_icon-icons.com_73701.png")));
		toolBar.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/Redo_icon-icons.com_73698.png")));
		toolBar.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/Cut_icon-icons.com_73697.png")));
		toolBar.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/copy_paste_documents_1580.png")));
		toolBar.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("");
		btnNewButton_9.setIcon(new ImageIcon(Ventana_Aplicacion.class.getResource("/images/copy_paste_document_file_1557.png")));
		toolBar.add(btnNewButton_9);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 63, 754, 403);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Usuario", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblCif = new JLabel("CIF:");
		lblCif.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCif.setBounds(70, 30, 46, 21);
		panel.add(lblCif);
		
		JLabel lblDireccin = new JLabel("DIRECCI\u00D3N:");
		lblDireccin.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDireccin.setBounds(70, 62, 80, 21);
		panel.add(lblDireccin);
		
		JLabel lblCp = new JLabel("CP:");
		lblCp.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCp.setBounds(70, 94, 46, 21);
		panel.add(lblCp);
		
		JLabel lblUsuario = new JLabel("USUARIO:");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUsuario.setBounds(70, 141, 80, 21);
		panel.add(lblUsuario);
		
		textField = new JTextField();
		textField.setBounds(197, 31, 161, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(197, 63, 161, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(197, 95, 103, 20);
		panel.add(textField_2);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("ALTA");
		chckbxNewCheckBox.setBackground(new Color(255, 153, 102));
		buttonGroup.add(chckbxNewCheckBox);
		chckbxNewCheckBox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckbxNewCheckBox.setBorderPainted(true);
		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.BOLD, 13));
		chckbxNewCheckBox.setBounds(227, 141, 80, 23);
		panel.add(chckbxNewCheckBox);
		
		JCheckBox chckbxBaja = new JCheckBox("BAJA");
		chckbxBaja.setBackground(new Color(255, 153, 102));
		chckbxBaja.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup.add(chckbxBaja);
		chckbxBaja.setBorderPainted(true);
		chckbxBaja.setFont(new Font("Tahoma", Font.BOLD, 13));
		chckbxBaja.setBounds(320, 140, 80, 23);
		panel.add(chckbxBaja);
		
		JCheckBox chckbxModificacin = new JCheckBox("MODIFICACI\u00D3N");
		chckbxModificacin.setBackground(new Color(255, 153, 102));
		chckbxModificacin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup.add(chckbxModificacin);
		chckbxModificacin.setBorderPainted(true);
		chckbxModificacin.setFont(new Font("Tahoma", Font.BOLD, 13));
		chckbxModificacin.setBounds(416, 140, 127, 23);
		panel.add(chckbxModificacin);
		
		JLabel lblFecha = new JLabel("FECHA:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblFecha.setBounds(497, 30, 53, 21);
		panel.add(lblFecha);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1549695600000L), null, null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(576, 31, 92, 20);
		panel.add(spinner);
		
		JLabel lblNombre = new JLabel("NOMBRE:");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNombre.setBounds(70, 184, 80, 21);
		panel.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("APELLIDOS:");
		lblApellidos.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblApellidos.setBounds(70, 216, 80, 21);
		panel.add(lblApellidos);
		
		JLabel lblNif = new JLabel("NIF:");
		lblNif.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNif.setBounds(70, 248, 80, 21);
		panel.add(lblNif);
		
		JLabel lblTelefono = new JLabel("TELEFONO:");
		lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTelefono.setBounds(70, 280, 80, 21);
		panel.add(lblTelefono);
		
		JLabel lblFax = new JLabel("EMAIL:");
		lblFax.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblFax.setBounds(416, 184, 80, 21);
		panel.add(lblFax);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(197, 185, 161, 20);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(197, 217, 161, 20);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(197, 249, 127, 20);
		panel.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(197, 281, 127, 20);
		panel.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(529, 185, 154, 20);
		panel.add(textField_7);
		
		JLabel lblExtensin = new JLabel("FAX:");
		lblExtensin.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblExtensin.setBounds(416, 216, 80, 21);
		panel.add(lblExtensin);
		
		JLabel lblEmail = new JLabel("EXTENSI\u00D3N");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEmail.setBounds(416, 248, 80, 21);
		panel.add(lblEmail);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(529, 217, 127, 20);
		panel.add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(529, 249, 92, 20);
		panel.add(textField_9);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCancelar.setBounds(631, 326, 105, 23);
		panel.add(btnCancelar);
		
		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnAceptar.setBounds(516, 327, 105, 23);
		panel.add(btnAceptar);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Veh\u00EDculo", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblMatr = new JLabel("MATR\u00CDCULA:");
		lblMatr.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMatr.setBounds(70, 30, 80, 21);
		panel_1.add(lblMatr);
		
		JLabel lblPropietario = new JLabel("PROPIETARIO:");
		lblPropietario.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPropietario.setBounds(70, 62, 100, 21);
		panel_1.add(lblPropietario);
		
		JLabel label = new JLabel("FECHA:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(462, 30, 53, 21);
		panel_1.add(label);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(210, 31, 161, 20);
		panel_1.add(textField_10);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(210, 63, 161, 20);
		panel_1.add(textField_11);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBackground(new Color(255, 255, 255));
		spinner_1.setModel(new SpinnerDateModel(new Date(1550041200000L), null, null, Calendar.DAY_OF_YEAR));
		spinner_1.setBounds(562, 31, 100, 20);
		panel_1.add(spinner_1);
		
		JLabel lblVehculo = new JLabel("VEH\u00CDCULO:");
		lblVehculo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblVehculo.setBounds(70, 117, 80, 21);
		panel_1.add(lblVehculo);
		
		JRadioButton rdbtnBaja = new JRadioButton("BAJA");
		rdbtnBaja.setBackground(new Color(255, 153, 102));
		rdbtnBaja.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnBaja.setBorderPainted(true);
		buttonGroup_1.add(rdbtnBaja);
		rdbtnBaja.setFont(new Font("Tahoma", Font.BOLD, 13));
		rdbtnBaja.setBounds(302, 116, 65, 23);
		panel_1.add(rdbtnBaja);
		
		JRadioButton rdbtnAlta = new JRadioButton("ALTA");
		rdbtnAlta.setBackground(new Color(255, 153, 102));
		rdbtnAlta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnAlta.setBorderPainted(true);
		buttonGroup_1.add(rdbtnAlta);
		rdbtnAlta.setFont(new Font("Tahoma", Font.BOLD, 13));
		rdbtnAlta.setBounds(235, 116, 65, 23);
		panel_1.add(rdbtnAlta);
		
		JRadioButton rdbtnModificacin = new JRadioButton("MODIFICACI\u00D3N");
		rdbtnModificacin.setBackground(new Color(255, 153, 102));
		rdbtnModificacin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnModificacin.setBorderPainted(true);
		buttonGroup_1.add(rdbtnModificacin);
		rdbtnModificacin.setFont(new Font("Tahoma", Font.BOLD, 13));
		rdbtnModificacin.setBounds(369, 117, 124, 23);
		panel_1.add(rdbtnModificacin);
		
		JLabel lblMarca = new JLabel("MARCA:");
		lblMarca.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMarca.setBounds(70, 173, 80, 21);
		panel_1.add(lblMarca);
		
		JLabel lblModelo = new JLabel("MODELO:");
		lblModelo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblModelo.setBounds(70, 205, 80, 21);
		panel_1.add(lblModelo);
		
		JLabel lblColor = new JLabel("COLOR:");
		lblColor.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblColor.setBounds(70, 237, 80, 21);
		panel_1.add(lblColor);
		
		JLabel lblAoItv = new JLabel("A\u00D1O ITV:");
		lblAoItv.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAoItv.setBounds(70, 269, 80, 21);
		panel_1.add(lblAoItv);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(210, 174, 161, 20);
		panel_1.add(textField_12);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(210, 206, 161, 20);
		panel_1.add(textField_13);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(210, 238, 161, 20);
		panel_1.add(textField_14);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		textField_15.setBounds(210, 270, 118, 20);
		panel_1.add(textField_15);
		
		JLabel lblHistorialMultas = new JLabel("HISTORIAL DE MULTAS:");
		lblHistorialMultas.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblHistorialMultas.setBounds(462, 159, 155, 21);
		panel_1.add(lblHistorialMultas);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(462, 187, 238, 103);
		panel_1.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(new Color(255, 255, 204));
		scrollPane.setViewportView(textArea);
		
		JButton button = new JButton("ACEPTAR");
		button.setFont(new Font("Tahoma", Font.BOLD, 12));
		button.setBounds(485, 328, 105, 23);
		panel_1.add(button);
		
		JButton btnCancelar_1 = new JButton("CANCELAR");
		btnCancelar_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCancelar_1.setBounds(600, 328, 123, 23);
		panel_1.add(btnCancelar_1);
	}
}
